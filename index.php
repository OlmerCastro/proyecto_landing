<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Landing page</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <header>
        <img src="imagenes/logo2.png" alt="Logotipo del Marketplace">
    </header>
    <div id="banner-landing">
        <img src="imagenes/Promocionesmensuales.png" alt="Imagen de promoción por temporada">
    </div>    
    <div id="contenedor-titulo">   
        <h1>Productos de colección</h1>            
    </div>
    <?php
        $productos = ["Maracas llaneras", "Instrumento musicales", "Carne a la llanera"];
        $precios = ["$100.00 C/u", "$500.00 C/u", "$50.00 C/u"];
    ?>
    <div id="contenedor-tarjetas">
        <div class="tarjeta">
            <img src="imagenes/Artesanía.jpg" alt="imágenes de maracas llaneras" height="250">
            <h3>
                <?php
                    echo $productos[0];
                ?>                
            </h3>
            <h4>
            <?php
                    echo $precios[0];
                ?>
            </h4>
            <button>COMPRAR</button>
        </div>
        <div class="tarjeta">
            <img src="imagenes/Instrumentos.jpg" alt="Imágenes de instrumentos musicales"height="250">
            <h3>
                <?php
                    echo $productos[1];
                ?>                
            </h3>
            <h4>
            <?php
                    echo $precios[1];
                ?>
            </h4>
            <button>COMPRAR</button>
        </div>
        <div class="tarjeta">                
            <img src="imagenes/Comidas.jpg" alt="Imágenes de carne a la llanera "height="250">
            <h3> 
                <?php
                    echo $productos[2];
                ?>                
            </h3>
            <h4>
            <?php
                    echo $precios[2];
                ?>
            </h4>
            <button>COMPRAR</button>              
        </div>
    </div>        
    
    <div id="contenedor-formulario"></div>
        
        <form class="formulario-contacto" action="proceso.php" method="POST">
           
            <label for="nombre"><b>Nombre:</b></label>
            <input type="text" id="nombre" name="nombre" placeholder="Escriba su nombre">
            <br><br>

            <label for="nombre"><b>Teléfono:</b></label>
            <input type="text" id="telefono" name="telefono" placeholder="Escriba su número de teléfono">
            <br><br>

            <label for="correo"><b>Correo:</b></label>
            <input type="text" id="correo" name="correo" placeholder="Escriba su correo">
            <br><br>

            <label for="asunto"><b>Mensaje:</b></label>
            <textarea name="asunto" id="" cols="60" rows="3" placeholder="Escriba su mensaje"></textarea>
            <br><br>
            
            <input type="submit" value="ENVIAR">
            <!--<button>ENVIAR</button> -->
        </form>
        <br><br>
           
</body>
</html>